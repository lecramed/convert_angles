# ddCdms()
# author: Marcel Brown
# 2020-01-28
#
# Convert degrees minutes seconds to decimal degrees.

# Imports
from re import split


def dmsCdd(dms_angle):
    '''
    Input an angle in DMS, either vector [D,M,S] or string (-)D°M'S", get
    decimal degrees. For negatives use: [-D,M,S], [0,-M,S], or [0,0,-S].
    '''
    # Constants
    #d_separator_input = ['°', 'd', 'D', '^', "'", '"', ',']
    #m_separator_input = ["'", ',']
    #s_separator_input = ['"', "'", ',']

    # Parse based on input format.
    if type(dms_angle) is list:

        # TODO: Handle -0s here. Or, wait, should never be -0 here...

        degrees = dms_angle[0]
        minutes = dms_angle[1]
        seconds = dms_angle[2]
    elif dms_angle[0] is "[":
        # In the case of a "string list" we create a list of strings by removing the
        # brackets and splitting on the commas.
        dms_vector_gen = split(
            "[,,]+", dms_angle.replace('[', '').replace(']', ''))

        # TODO: Handle -0s here. Using float seems to handle the first bit anyway...

        degrees = float(dms_vector_gen[0])
        minutes = float(dms_vector_gen[1])
        seconds = float(dms_vector_gen[2])
    else:
        # First strip any whitespace.
        dms_angle = dms_angle.replace(' ', '')

        # Use regex to split the string and return the parts as a list. Breaking this down:
        # "[ ]" --> this encloses the regex characters to split on (and delete).
        # The + skips one or more split characters in a row (solves e.g. '' instead of ").
        #  °dD^'\", --> the characters to split on, except the \ which escapes the ".
        dms_angle_gen = split("[°dD^'\",]+", dms_angle)
        # Drop the last item in the list in cases where there is a trailing separator. This
        # occurs in most every case because the seconds separator (e.g. ") is parsed and
        # results in a fourth '' after the split.
        if dms_angle_gen[-1] is '':
            dms_angle_gen = dms_angle_gen[:-1]

        # TODO: handle only degrees, only minutes, only seconds.

        # TODO: Handle -0s here.

        degrees = float(dms_angle_gen[0])
        minutes = float(dms_angle_gen[1])
        seconds = float(dms_angle_gen[2])

    # Convert to decimal degrees.
    dd_angle = degrees + (minutes / 60) + (seconds / 3600)
    return dd_angle


if __name__ == '__main__':
    print('Enter an angle in degrees minutes seconds. It can be a vector of the forms:')
    print('[D,M,S], [-D,M,S], [0,-M,S], [0,0,-S], or a string separated with any:')
    dms_angle = input("combination of ° d D ^ ' \" or commas. ")
    dd_angle = dmsCdd(dms_angle)
    print(dd_angle)
    # Prevent terminals from closing when run from file managers.
    exit = "do_not_exit"
    while exit is "do_not_exit":
        exit = input("\npress any key to exit")
