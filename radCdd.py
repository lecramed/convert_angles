# radCdd()
# author: Marcel Brown
# 2020-02-03
#
# Convert radians to decimal degrees

# Imports
from math import degrees

def radCdd(rad_angle):
    '''
    Input an angle in radians and get decimal degrees.
    '''
    return degrees(float(rad_angle))


if __name__ == '__main__':
    rad_angle = input("Enter an angle in radians: ")
    dd_angle = radCdd(rad_angle)
    print(dd_angle)
    # Prevent terminals from closing when run from file managers.
    exit = "do_not_exit"
    while exit is "do_not_exit":
        exit = input("\npress any key to exit")
