# ddCdms()
# author: Marcel Brown
# 2020-01-28
#
# Convert decimal degrees to degrees minutes seconds.


def ddCdms(dd_angle):
    '''
    Input an angle in decimal degrees, get DMS results [D,M,S] and 'DMS'.
    May result in: [-D,M,S], [0,-M,S], or [0,0,-S] for small negatives.
    '''
    # Constants
    d_separator = '°'  # common alternates: ^ d D ' ,
    m_separator = "'"
    s_separator = '"'

    # Convert the string input to float.
    dd_angle = float(dd_angle)
    # Truncate the decimal using int to get the degrees value.
    degrees = int(dd_angle)
    # Begin handling the special case for -0.x.
    if (degrees == 0) and (dd_angle < 0):
        signed_zero = True
    else:
        signed_zero = False
    # Get the absolute value of the dd angle (in case of a negative), subtract
    # to get only the values after the decimal, and convert to minutes.
    minutes_full = (abs(dd_angle) - abs(degrees)) * 60
    # Truncate to get the minutes.
    minutes = int(minutes_full)
    # Calculate the full float value for seconds.
    seconds = (minutes_full - minutes) * 60
    # Handle the -0.x case by making either the minutes or seconds negative.
    if signed_zero:
        # Handle string output (must be here or minutes/seconds will be negative):
        dms_string_sign = '-'
        dms_string = f'{dms_string_sign}{degrees}{d_separator}{minutes}{m_separator}{seconds}{s_separator}'
        # Handle vector output:
        if minutes != 0:
            minutes = -1 * minutes
        else:
            seconds = -1 * seconds
    else:
        # When we don't have a -0.x, create the string normally.
        dms_string = f'{degrees}{d_separator}{minutes}{m_separator}{seconds}{s_separator}'

    dms_vector = [degrees, minutes, seconds]

    return dms_vector, dms_string


if __name__ == '__main__':
    dd_angle = input("Input an angle in decimal degrees: ")
    dms_vector, dms_string = ddCdms(dd_angle)
    print(dms_vector)
    print(dms_string)
    # Prevent terminals from closing when run from file managers.
    exit = "do_not_exit"
    while exit is "do_not_exit":
        exit = input("\npress any key to exit")
