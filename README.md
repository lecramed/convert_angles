# convert_angles

Angle conversion tools written in Python. Designed for surveyors.

**Not ready for use.**

Will perform the following functions:
- Convert degrees minutes seconds (DMS) to decimal degrees (dd) and the reverse.
  - Handles negative decimal angles smaller than zero.
  - Accepts and outputs DMS vectors and all common DMS text strings.
  - Converts text files and csv files for batch processing.
- Convert latitude and longitude values.
  - Option for longitude positive west.
- Convert azimuths to bearings and the reverse.