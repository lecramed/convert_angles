# ddCrad()
# author: Marcel Brown
# 2020-02-03
#
# Convert decimal degrees to radians

# Imports
from math import radians

def ddCrad(dd_angle):
    '''
    Input an angle in decimal degrees and get radians.
    '''
    return radians(float(dd_angle))


if __name__ == '__main__':
    dd_angle = input("Enter an angle in decimal degrees: ")
    rad_angle = ddCrad(dd_angle)
    print(rad_angle)
    # Prevent terminals from closing when run from file managers.
    exit = "do_not_exit"
    while exit is "do_not_exit":
        exit = input("\npress any key to exit")
